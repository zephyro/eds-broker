
$(".btn_modal").fancybox({
    'padding'    : 0
});



// Lang

(function() {

    $('.header__lng_active').on('click touchstart', function(e){
        e.preventDefault();

        if($(this).closest('.header__lng').hasClass('open')){
            $(this).closest('.header__lng').toggleClass('open');
        }
        else {
            $('.header__lng').removeClass('open');
            $(this).closest('.header__lng').toggleClass('open');
        }
    });

}());

// Hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".header__lng").length === 0) {
        $(".header__lng").removeClass('open');
    }

});